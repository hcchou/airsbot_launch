#!/bin/sh

# USB device name.
script_dir=$(cd $(dirname $0);pwd)
dir=$(dirname $script_dir)
sudo cp "$dir/config/usb_dev.rules" /etc/udev/rules.d/.
sudo cp "$dir/config/99-obsensor-libusb.rules" /etc/udev/rules.d/99-obsensor-libusb.rules
sudo service udev reload
sudo service udev restart
